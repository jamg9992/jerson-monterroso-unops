a.

SELECT c.descripcion_clasificacion, COUNT(i.id_incidentes), SUM(i.horas_soporte)
FROM incidentes i JOIN clasificacion c ON (i.id_clasificacion = i.id_clasificacion)
GROUP BY c.descripcion_clasificacion

b. 

SELECT *
FROM incidentes i JOIN estado e ON (i.id_estado = e.id_estado) 
WHERE e.descripcion_estado = 'Pendiente'

c.

SELECT i.nombre prioridad, MONTH(i.fecha), COUNT(i.id_incidentes), 
FROM incidentes i JOIN perfil p ON (i.id_perfil = p.id_perfil)
JOIN prioridad pr ON (pr.id_prioridad = i.id_prioridad)
GROUP BY i.nombre_prioridad, MONTH(i.fecha)

d.

SELECT i.nombre_perfil, COUNT(i.id_incidentes)
FROM incidentes i JOIN perfi p ON (i.id_perfil = p.id_perfil)
WHERE COUNT(i.id_incidentes) > 10
GROUP BY i.nombre_perfil

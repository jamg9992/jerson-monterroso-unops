<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Editing Estado</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Editing Estado</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdEstado:"/>
                    <h:outputText value="#{estado.estado.idEstado}" title="IdEstado" />
                    <h:outputText value="DescripcionEstado:"/>
                    <h:inputText id="descripcionEstado" value="#{estado.estado.descripcionEstado}" title="DescripcionEstado" />
                    <h:outputText value="IncidenteCollection:"/>
                    <h:selectManyListbox id="incidenteCollection" value="#{estado.estado.jsfcrud_transform[jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.collectionToArray][jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.arrayToList].incidenteCollection}" title="IncidenteCollection" size="6" converter="#{incidente.converter}" >
                        <f:selectItems value="#{incidente.incidenteItemsAvailableSelectMany}"/>
                    </h:selectManyListbox>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{estado.edit}" value="Save">
                    <f:param name="jsfcrud.currentEstado" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][estado.estado][estado.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{estado.detailSetup}" value="Show" immediate="true">
                    <f:param name="jsfcrud.currentEstado" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][estado.estado][estado.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <h:commandLink action="#{estado.listSetup}" value="Show All Estado Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

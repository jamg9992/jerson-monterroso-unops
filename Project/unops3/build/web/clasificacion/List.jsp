<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Listing Clasificacion Items</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Listing Clasificacion Items</h1>
            <h:form styleClass="jsfcrud_list_form">
                <h:outputText escape="false" value="(No Clasificacion Items Found)<br />" rendered="#{clasificacion.pagingInfo.itemCount == 0}" />
                <h:panelGroup rendered="#{clasificacion.pagingInfo.itemCount > 0}">
                    <h:outputText value="Item #{clasificacion.pagingInfo.firstItem + 1}..#{clasificacion.pagingInfo.lastItem} of #{clasificacion.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{clasificacion.prev}" value="Previous #{clasificacion.pagingInfo.batchSize}" rendered="#{clasificacion.pagingInfo.firstItem >= clasificacion.pagingInfo.batchSize}"/>&nbsp;
                    <h:commandLink action="#{clasificacion.next}" value="Next #{clasificacion.pagingInfo.batchSize}" rendered="#{clasificacion.pagingInfo.lastItem + clasificacion.pagingInfo.batchSize <= clasificacion.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{clasificacion.next}" value="Remaining #{clasificacion.pagingInfo.itemCount - clasificacion.pagingInfo.lastItem}"
                                   rendered="#{clasificacion.pagingInfo.lastItem < clasificacion.pagingInfo.itemCount && clasificacion.pagingInfo.lastItem + clasificacion.pagingInfo.batchSize > clasificacion.pagingInfo.itemCount}"/>
                    <h:dataTable value="#{clasificacion.clasificacionItems}" var="item" border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px">
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="IdClasificacion"/>
                            </f:facet>
                            <h:outputText value="#{item.idClasificacion}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="DescripcionClasificacion"/>
                            </f:facet>
                            <h:outputText value="#{item.descripcionClasificacion}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText escape="false" value="&nbsp;"/>
                            </f:facet>
                            <h:commandLink value="Show" action="#{clasificacion.detailSetup}">
                                <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][clasificacion.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{clasificacion.editSetup}">
                                <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][clasificacion.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{clasificacion.remove}">
                                <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][clasificacion.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                        </h:column>

                    </h:dataTable>
                </h:panelGroup>
                <br />
                <h:commandLink action="#{clasificacion.createSetup}" value="New Clasificacion"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />


            </h:form>
        </body>
    </html>
</f:view>

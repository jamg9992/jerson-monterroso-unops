<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Clasificacion Detail</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Clasificacion Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdClasificacion:"/>
                    <h:outputText value="#{clasificacion.clasificacion.idClasificacion}" title="IdClasificacion" />
                    <h:outputText value="DescripcionClasificacion:"/>
                    <h:outputText value="#{clasificacion.clasificacion.descripcionClasificacion}" title="DescripcionClasificacion" />

                    <h:outputText value="IncidenteCollection:" />
                    <h:panelGroup>
                        <h:outputText rendered="#{empty clasificacion.clasificacion.incidenteCollection}" value="(No Items)"/>
                        <h:dataTable value="#{clasificacion.clasificacion.incidenteCollection}" var="item" 
                                     border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px" 
                                     rendered="#{not empty clasificacion.clasificacion.incidenteCollection}">
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdIncidente"/>
                                </f:facet>
                                <h:outputText value="#{item.idIncidente}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="Asunto"/>
                                </f:facet>
                                <h:outputText value="#{item.asunto}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="HorasSoporte"/>
                                </f:facet>
                                <h:outputText value="#{item.horasSoporte}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="Fecha"/>
                                </f:facet>
                                <h:outputText value="#{item.fecha}">
                                    <f:convertDateTime pattern="MM/dd/yyyy HH:mm:ss" />
                                </h:outputText>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdClasificacion"/>
                                </f:facet>
                                <h:outputText value="#{item.idClasificacion}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdEstado"/>
                                </f:facet>
                                <h:outputText value="#{item.idEstado}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdPrioridad"/>
                                </f:facet>
                                <h:outputText value="#{item.idPrioridad}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdUsuario"/>
                                </f:facet>
                                <h:outputText value="#{item.idUsuario}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText escape="false" value="&nbsp;"/>
                                </f:facet>
                                <h:commandLink value="Show" action="#{incidente.detailSetup}">
                                    <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][clasificacion.clasificacion][clasificacion.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="clasificacion" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.ClasificacionController" />
                                </h:commandLink>
                                <h:outputText value=" "/>
                                <h:commandLink value="Edit" action="#{incidente.editSetup}">
                                    <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][clasificacion.clasificacion][clasificacion.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="clasificacion" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.ClasificacionController" />
                                </h:commandLink>
                                <h:outputText value=" "/>
                                <h:commandLink value="Destroy" action="#{incidente.destroy}">
                                    <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][clasificacion.clasificacion][clasificacion.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="clasificacion" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.ClasificacionController" />
                                </h:commandLink>
                            </h:column>
                        </h:dataTable>
                    </h:panelGroup>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{clasificacion.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][clasificacion.clasificacion][clasificacion.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{clasificacion.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][clasificacion.clasificacion][clasificacion.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{clasificacion.createSetup}" value="New Clasificacion" />
                <br />
                <h:commandLink action="#{clasificacion.listSetup}" value="Show All Clasificacion Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

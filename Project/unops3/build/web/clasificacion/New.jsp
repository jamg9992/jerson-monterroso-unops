<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>New Clasificacion</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>New Clasificacion</h1>
            <h:form>
                <h:inputHidden id="validateCreateField" validator="#{clasificacion.validateCreate}" value="value"/>
                <h:panelGrid columns="2">
                    <h:outputText value="IdClasificacion:"/>
                    <h:inputText id="idClasificacion" value="#{clasificacion.clasificacion.idClasificacion}" title="IdClasificacion" required="true" requiredMessage="The idClasificacion field is required." />
                    <h:outputText value="DescripcionClasificacion:"/>
                    <h:inputText id="descripcionClasificacion" value="#{clasificacion.clasificacion.descripcionClasificacion}" title="DescripcionClasificacion" />
                    <h:outputText value="IncidenteCollection:"/>
                    <h:selectManyListbox id="incidenteCollection" value="#{clasificacion.clasificacion.jsfcrud_transform[jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.collectionToArray][jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.arrayToList].incidenteCollection}" title="IncidenteCollection" size="6" converter="#{incidente.converter}" >
                        <f:selectItems value="#{incidente.incidenteItemsAvailableSelectMany}"/>
                    </h:selectManyListbox>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{clasificacion.create}" value="Create"/>
                <br />
                <br />
                <h:commandLink action="#{clasificacion.listSetup}" value="Show All Clasificacion Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

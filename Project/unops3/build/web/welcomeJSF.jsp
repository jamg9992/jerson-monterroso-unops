<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="f" uri="http://java.sun.com/jsf/core"%>
<%@taglib prefix="h" uri="http://java.sun.com/jsf/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%--
    This file is an entry point for JavaServer Faces application.
--%>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
            <title>JSP Page</title>
<link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h1><h:outputText value="JavaServer Faces"/></h1>
                <h:form>
                    <h:commandLink action="#{usuario.listSetup}" value="Show All Usuario Items"/>
                </h:form>

                <h:form>
                    <h:commandLink action="#{prioridad.listSetup}" value="Show All Prioridad Items"/>
                </h:form>

                <h:form>
                    <h:commandLink action="#{perfil.listSetup}" value="Show All Perfil Items"/>
                </h:form>

                <h:form>
                    <h:commandLink action="#{incidente.listSetup}" value="Show All Incidente Items"/>
                </h:form>

                <h:form>
                    <h:commandLink action="#{estado.listSetup}" value="Show All Estado Items"/>
                </h:form>

                <h:form>
                    <h:commandLink action="#{clasificacion.listSetup}" value="Show All Clasificacion Items"/>
                </h:form>

        </body>
    </html>
</f:view>

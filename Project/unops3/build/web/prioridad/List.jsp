<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Listing Prioridad Items</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Listing Prioridad Items</h1>
            <h:form styleClass="jsfcrud_list_form">
                <h:outputText escape="false" value="(No Prioridad Items Found)<br />" rendered="#{prioridad.pagingInfo.itemCount == 0}" />
                <h:panelGroup rendered="#{prioridad.pagingInfo.itemCount > 0}">
                    <h:outputText value="Item #{prioridad.pagingInfo.firstItem + 1}..#{prioridad.pagingInfo.lastItem} of #{prioridad.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{prioridad.prev}" value="Previous #{prioridad.pagingInfo.batchSize}" rendered="#{prioridad.pagingInfo.firstItem >= prioridad.pagingInfo.batchSize}"/>&nbsp;
                    <h:commandLink action="#{prioridad.next}" value="Next #{prioridad.pagingInfo.batchSize}" rendered="#{prioridad.pagingInfo.lastItem + prioridad.pagingInfo.batchSize <= prioridad.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{prioridad.next}" value="Remaining #{prioridad.pagingInfo.itemCount - prioridad.pagingInfo.lastItem}"
                                   rendered="#{prioridad.pagingInfo.lastItem < prioridad.pagingInfo.itemCount && prioridad.pagingInfo.lastItem + prioridad.pagingInfo.batchSize > prioridad.pagingInfo.itemCount}"/>
                    <h:dataTable value="#{prioridad.prioridadItems}" var="item" border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px">
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="IdPrioridad"/>
                            </f:facet>
                            <h:outputText value="#{item.idPrioridad}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="NombrePrioridad"/>
                            </f:facet>
                            <h:outputText value="#{item.nombrePrioridad}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText escape="false" value="&nbsp;"/>
                            </f:facet>
                            <h:commandLink value="Show" action="#{prioridad.detailSetup}">
                                <f:param name="jsfcrud.currentPrioridad" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][prioridad.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{prioridad.editSetup}">
                                <f:param name="jsfcrud.currentPrioridad" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][prioridad.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{prioridad.remove}">
                                <f:param name="jsfcrud.currentPrioridad" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][prioridad.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                        </h:column>

                    </h:dataTable>
                </h:panelGroup>
                <br />
                <h:commandLink action="#{prioridad.createSetup}" value="New Prioridad"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />


            </h:form>
        </body>
    </html>
</f:view>

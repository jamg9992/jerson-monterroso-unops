<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>New Prioridad</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>New Prioridad</h1>
            <h:form>
                <h:inputHidden id="validateCreateField" validator="#{prioridad.validateCreate}" value="value"/>
                <h:panelGrid columns="2">
                    <h:outputText value="IdPrioridad:"/>
                    <h:inputText id="idPrioridad" value="#{prioridad.prioridad.idPrioridad}" title="IdPrioridad" required="true" requiredMessage="The idPrioridad field is required." />
                    <h:outputText value="NombrePrioridad:"/>
                    <h:inputText id="nombrePrioridad" value="#{prioridad.prioridad.nombrePrioridad}" title="NombrePrioridad" />
                    <h:outputText value="IncidenteCollection:"/>
                    <h:selectManyListbox id="incidenteCollection" value="#{prioridad.prioridad.jsfcrud_transform[jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.collectionToArray][jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.arrayToList].incidenteCollection}" title="IncidenteCollection" size="6" converter="#{incidente.converter}" >
                        <f:selectItems value="#{incidente.incidenteItemsAvailableSelectMany}"/>
                    </h:selectManyListbox>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{prioridad.create}" value="Create"/>
                <br />
                <br />
                <h:commandLink action="#{prioridad.listSetup}" value="Show All Prioridad Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

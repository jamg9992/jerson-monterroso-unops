<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Editing Usuario</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Editing Usuario</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdUsuario:"/>
                    <h:outputText value="#{usuario.usuario.idUsuario}" title="IdUsuario" />
                    <h:outputText value="NombreUsuario:"/>
                    <h:inputText id="nombreUsuario" value="#{usuario.usuario.nombreUsuario}" title="NombreUsuario" />
                    <h:outputText value="IncidenteCollection:"/>
                    <h:selectManyListbox id="incidenteCollection" value="#{usuario.usuario.jsfcrud_transform[jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.collectionToArray][jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.arrayToList].incidenteCollection}" title="IncidenteCollection" size="6" converter="#{incidente.converter}" >
                        <f:selectItems value="#{incidente.incidenteItemsAvailableSelectMany}"/>
                    </h:selectManyListbox>
                    <h:outputText value="IdPerfil:"/>
                    <h:selectOneMenu id="idPerfil" value="#{usuario.usuario.idPerfil}" title="IdPerfil" required="true" requiredMessage="The idPerfil field is required." >
                        <f:selectItems value="#{perfil.perfilItemsAvailableSelectOne}"/>
                    </h:selectOneMenu>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{usuario.edit}" value="Save">
                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{usuario.detailSetup}" value="Show" immediate="true">
                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <h:commandLink action="#{usuario.listSetup}" value="Show All Usuario Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

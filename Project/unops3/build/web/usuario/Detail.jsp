<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Usuario Detail</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Usuario Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdUsuario:"/>
                    <h:outputText value="#{usuario.usuario.idUsuario}" title="IdUsuario" />
                    <h:outputText value="NombreUsuario:"/>
                    <h:outputText value="#{usuario.usuario.nombreUsuario}" title="NombreUsuario" />
                    <h:outputText value="IdPerfil:"/>
                    <h:panelGroup>
                        <h:outputText value="#{usuario.usuario.idPerfil}"/>
                        <h:panelGroup rendered="#{usuario.usuario.idPerfil != null}">
                            <h:outputText value=" ("/>
                            <h:commandLink value="Show" action="#{perfil.detailSetup}">
                                <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario.idPerfil][perfil.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="usuario"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.UsuarioController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{perfil.editSetup}">
                                <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario.idPerfil][perfil.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="usuario"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.UsuarioController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{perfil.destroy}">
                                <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario.idPerfil][perfil.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="usuario"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.UsuarioController"/>
                            </h:commandLink>
                            <h:outputText value=" )"/>
                        </h:panelGroup>
                    </h:panelGroup>

                    <h:outputText value="IncidenteCollection:" />
                    <h:panelGroup>
                        <h:outputText rendered="#{empty usuario.usuario.incidenteCollection}" value="(No Items)"/>
                        <h:dataTable value="#{usuario.usuario.incidenteCollection}" var="item" 
                                     border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px" 
                                     rendered="#{not empty usuario.usuario.incidenteCollection}">
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdIncidente"/>
                                </f:facet>
                                <h:outputText value="#{item.idIncidente}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="Asunto"/>
                                </f:facet>
                                <h:outputText value="#{item.asunto}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="HorasSoporte"/>
                                </f:facet>
                                <h:outputText value="#{item.horasSoporte}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="Fecha"/>
                                </f:facet>
                                <h:outputText value="#{item.fecha}">
                                    <f:convertDateTime pattern="MM/dd/yyyy HH:mm:ss" />
                                </h:outputText>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdClasificacion"/>
                                </f:facet>
                                <h:outputText value="#{item.idClasificacion}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdEstado"/>
                                </f:facet>
                                <h:outputText value="#{item.idEstado}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdPrioridad"/>
                                </f:facet>
                                <h:outputText value="#{item.idPrioridad}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdUsuario"/>
                                </f:facet>
                                <h:outputText value="#{item.idUsuario}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText escape="false" value="&nbsp;"/>
                                </f:facet>
                                <h:commandLink value="Show" action="#{incidente.detailSetup}">
                                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="usuario" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.UsuarioController" />
                                </h:commandLink>
                                <h:outputText value=" "/>
                                <h:commandLink value="Edit" action="#{incidente.editSetup}">
                                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="usuario" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.UsuarioController" />
                                </h:commandLink>
                                <h:outputText value=" "/>
                                <h:commandLink value="Destroy" action="#{incidente.destroy}">
                                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="usuario" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.UsuarioController" />
                                </h:commandLink>
                            </h:column>
                        </h:dataTable>
                    </h:panelGroup>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{usuario.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{usuario.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][usuario.usuario][usuario.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{usuario.createSetup}" value="New Usuario" />
                <br />
                <h:commandLink action="#{usuario.listSetup}" value="Show All Usuario Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

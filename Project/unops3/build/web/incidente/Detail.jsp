<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Incidente Detail</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Incidente Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdIncidente:"/>
                    <h:outputText value="#{incidente.incidente.idIncidente}" title="IdIncidente" />
                    <h:outputText value="Asunto:"/>
                    <h:outputText value="#{incidente.incidente.asunto}" title="Asunto" />
                    <h:outputText value="HorasSoporte:"/>
                    <h:outputText value="#{incidente.incidente.horasSoporte}" title="HorasSoporte" />
                    <h:outputText value="Fecha:"/>
                    <h:outputText value="#{incidente.incidente.fecha}" title="Fecha" >
                        <f:convertDateTime pattern="MM/dd/yyyy HH:mm:ss" />
                    </h:outputText>
                    <h:outputText value="IdClasificacion:"/>
                    <h:panelGroup>
                        <h:outputText value="#{incidente.incidente.idClasificacion}"/>
                        <h:panelGroup rendered="#{incidente.incidente.idClasificacion != null}">
                            <h:outputText value=" ("/>
                            <h:commandLink value="Show" action="#{clasificacion.detailSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idClasificacion][clasificacion.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{clasificacion.editSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idClasificacion][clasificacion.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{clasificacion.destroy}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentClasificacion" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idClasificacion][clasificacion.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" )"/>
                        </h:panelGroup>
                    </h:panelGroup>
                    <h:outputText value="IdEstado:"/>
                    <h:panelGroup>
                        <h:outputText value="#{incidente.incidente.idEstado}"/>
                        <h:panelGroup rendered="#{incidente.incidente.idEstado != null}">
                            <h:outputText value=" ("/>
                            <h:commandLink value="Show" action="#{estado.detailSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentEstado" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idEstado][estado.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{estado.editSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentEstado" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idEstado][estado.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{estado.destroy}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentEstado" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idEstado][estado.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" )"/>
                        </h:panelGroup>
                    </h:panelGroup>
                    <h:outputText value="IdPrioridad:"/>
                    <h:panelGroup>
                        <h:outputText value="#{incidente.incidente.idPrioridad}"/>
                        <h:panelGroup rendered="#{incidente.incidente.idPrioridad != null}">
                            <h:outputText value=" ("/>
                            <h:commandLink value="Show" action="#{prioridad.detailSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentPrioridad" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idPrioridad][prioridad.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{prioridad.editSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentPrioridad" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idPrioridad][prioridad.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{prioridad.destroy}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentPrioridad" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idPrioridad][prioridad.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" )"/>
                        </h:panelGroup>
                    </h:panelGroup>
                    <h:outputText value="IdUsuario:"/>
                    <h:panelGroup>
                        <h:outputText value="#{incidente.incidente.idUsuario}"/>
                        <h:panelGroup rendered="#{incidente.incidente.idUsuario != null}">
                            <h:outputText value=" ("/>
                            <h:commandLink value="Show" action="#{usuario.detailSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idUsuario][usuario.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{usuario.editSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idUsuario][usuario.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{usuario.destroy}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente.idUsuario][usuario.converter].jsfcrud_invoke}"/>
                                <f:param name="jsfcrud.relatedController" value="incidente"/>
                                <f:param name="jsfcrud.relatedControllerType" value="unops34.IncidenteController"/>
                            </h:commandLink>
                            <h:outputText value=" )"/>
                        </h:panelGroup>
                    </h:panelGroup>


                </h:panelGrid>
                <br />
                <h:commandLink action="#{incidente.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{incidente.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{incidente.createSetup}" value="New Incidente" />
                <br />
                <h:commandLink action="#{incidente.listSetup}" value="Show All Incidente Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

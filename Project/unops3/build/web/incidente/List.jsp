<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Listing Incidente Items</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Listing Incidente Items</h1>
            <h:form styleClass="jsfcrud_list_form">
                <h:outputText escape="false" value="(No Incidente Items Found)<br />" rendered="#{incidente.pagingInfo.itemCount == 0}" />
                <h:panelGroup rendered="#{incidente.pagingInfo.itemCount > 0}">
                    <h:outputText value="Item #{incidente.pagingInfo.firstItem + 1}..#{incidente.pagingInfo.lastItem} of #{incidente.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{incidente.prev}" value="Previous #{incidente.pagingInfo.batchSize}" rendered="#{incidente.pagingInfo.firstItem >= incidente.pagingInfo.batchSize}"/>&nbsp;
                    <h:commandLink action="#{incidente.next}" value="Next #{incidente.pagingInfo.batchSize}" rendered="#{incidente.pagingInfo.lastItem + incidente.pagingInfo.batchSize <= incidente.pagingInfo.itemCount}"/>&nbsp;
                    <h:commandLink action="#{incidente.next}" value="Remaining #{incidente.pagingInfo.itemCount - incidente.pagingInfo.lastItem}"
                                   rendered="#{incidente.pagingInfo.lastItem < incidente.pagingInfo.itemCount && incidente.pagingInfo.lastItem + incidente.pagingInfo.batchSize > incidente.pagingInfo.itemCount}"/>
                    <h:dataTable value="#{incidente.incidenteItems}" var="item" border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px">
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="IdIncidente"/>
                            </f:facet>
                            <h:outputText value="#{item.idIncidente}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Asunto"/>
                            </f:facet>
                            <h:outputText value="#{item.asunto}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="HorasSoporte"/>
                            </f:facet>
                            <h:outputText value="#{item.horasSoporte}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="Fecha"/>
                            </f:facet>
                            <h:outputText value="#{item.fecha}">
                                <f:convertDateTime pattern="MM/dd/yyyy HH:mm:ss" />
                            </h:outputText>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="IdClasificacion"/>
                            </f:facet>
                            <h:outputText value="#{item.idClasificacion}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="IdEstado"/>
                            </f:facet>
                            <h:outputText value="#{item.idEstado}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="IdPrioridad"/>
                            </f:facet>
                            <h:outputText value="#{item.idPrioridad}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText value="IdUsuario"/>
                            </f:facet>
                            <h:outputText value="#{item.idUsuario}"/>
                        </h:column>
                        <h:column>
                            <f:facet name="header">
                                <h:outputText escape="false" value="&nbsp;"/>
                            </f:facet>
                            <h:commandLink value="Show" action="#{incidente.detailSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Edit" action="#{incidente.editSetup}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                            <h:outputText value=" "/>
                            <h:commandLink value="Destroy" action="#{incidente.remove}">
                                <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][incidente.converter].jsfcrud_invoke}"/>
                            </h:commandLink>
                        </h:column>

                    </h:dataTable>
                </h:panelGroup>
                <br />
                <h:commandLink action="#{incidente.createSetup}" value="New Incidente"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />


            </h:form>
        </body>
    </html>
</f:view>

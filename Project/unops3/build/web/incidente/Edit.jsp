<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Editing Incidente</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Editing Incidente</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdIncidente:"/>
                    <h:outputText value="#{incidente.incidente.idIncidente}" title="IdIncidente" />
                    <h:outputText value="Asunto:"/>
                    <h:inputText id="asunto" value="#{incidente.incidente.asunto}" title="Asunto" />
                    <h:outputText value="HorasSoporte:"/>
                    <h:inputText id="horasSoporte" value="#{incidente.incidente.horasSoporte}" title="HorasSoporte" />
                    <h:outputText value="Fecha (MM/dd/yyyy HH:mm:ss):"/>
                    <h:inputText id="fecha" value="#{incidente.incidente.fecha}" title="Fecha" >
                        <f:convertDateTime pattern="MM/dd/yyyy HH:mm:ss" />
                    </h:inputText>
                    <h:outputText value="IdClasificacion:"/>
                    <h:selectOneMenu id="idClasificacion" value="#{incidente.incidente.idClasificacion}" title="IdClasificacion" required="true" requiredMessage="The idClasificacion field is required." >
                        <f:selectItems value="#{clasificacion.clasificacionItemsAvailableSelectOne}"/>
                    </h:selectOneMenu>
                    <h:outputText value="IdEstado:"/>
                    <h:selectOneMenu id="idEstado" value="#{incidente.incidente.idEstado}" title="IdEstado" required="true" requiredMessage="The idEstado field is required." >
                        <f:selectItems value="#{estado.estadoItemsAvailableSelectOne}"/>
                    </h:selectOneMenu>
                    <h:outputText value="IdPrioridad:"/>
                    <h:selectOneMenu id="idPrioridad" value="#{incidente.incidente.idPrioridad}" title="IdPrioridad" required="true" requiredMessage="The idPrioridad field is required." >
                        <f:selectItems value="#{prioridad.prioridadItemsAvailableSelectOne}"/>
                    </h:selectOneMenu>
                    <h:outputText value="IdUsuario:"/>
                    <h:selectOneMenu id="idUsuario" value="#{incidente.incidente.idUsuario}" title="IdUsuario" required="true" requiredMessage="The idUsuario field is required." >
                        <f:selectItems value="#{usuario.usuarioItemsAvailableSelectOne}"/>
                    </h:selectOneMenu>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{incidente.edit}" value="Save">
                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{incidente.detailSetup}" value="Show" immediate="true">
                    <f:param name="jsfcrud.currentIncidente" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][incidente.incidente][incidente.converter].jsfcrud_invoke}"/>
                </h:commandLink>
                <br />
                <h:commandLink action="#{incidente.listSetup}" value="Show All Incidente Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

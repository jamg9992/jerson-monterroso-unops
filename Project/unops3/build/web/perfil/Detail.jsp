<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>Perfil Detail</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>Perfil Detail</h1>
            <h:form>
                <h:panelGrid columns="2">
                    <h:outputText value="IdPerfil:"/>
                    <h:outputText value="#{perfil.perfil.idPerfil}" title="IdPerfil" />
                    <h:outputText value="Descripcion:"/>
                    <h:outputText value="#{perfil.perfil.descripcion}" title="Descripcion" />

                    <h:outputText value="UsuarioCollection:" />
                    <h:panelGroup>
                        <h:outputText rendered="#{empty perfil.perfil.usuarioCollection}" value="(No Items)"/>
                        <h:dataTable value="#{perfil.perfil.usuarioCollection}" var="item" 
                                     border="0" cellpadding="2" cellspacing="0" rowClasses="jsfcrud_odd_row,jsfcrud_even_row" rules="all" style="border:solid 1px" 
                                     rendered="#{not empty perfil.perfil.usuarioCollection}">
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdUsuario"/>
                                </f:facet>
                                <h:outputText value="#{item.idUsuario}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="NombreUsuario"/>
                                </f:facet>
                                <h:outputText value="#{item.nombreUsuario}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText value="IdPerfil"/>
                                </f:facet>
                                <h:outputText value="#{item.idPerfil}"/>
                            </h:column>
                            <h:column>
                                <f:facet name="header">
                                    <h:outputText escape="false" value="&nbsp;"/>
                                </f:facet>
                                <h:commandLink value="Show" action="#{usuario.detailSetup}">
                                    <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][perfil.perfil][perfil.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][usuario.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="perfil" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.PerfilController" />
                                </h:commandLink>
                                <h:outputText value=" "/>
                                <h:commandLink value="Edit" action="#{usuario.editSetup}">
                                    <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][perfil.perfil][perfil.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][usuario.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="perfil" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.PerfilController" />
                                </h:commandLink>
                                <h:outputText value=" "/>
                                <h:commandLink value="Destroy" action="#{usuario.destroy}">
                                    <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][perfil.perfil][perfil.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.currentUsuario" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][item][usuario.converter].jsfcrud_invoke}"/>
                                    <f:param name="jsfcrud.relatedController" value="perfil" />
                                    <f:param name="jsfcrud.relatedControllerType" value="unops34.PerfilController" />
                                </h:commandLink>
                            </h:column>
                        </h:dataTable>
                    </h:panelGroup>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{perfil.remove}" value="Destroy">
                    <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][perfil.perfil][perfil.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <br />
                <h:commandLink action="#{perfil.editSetup}" value="Edit">
                    <f:param name="jsfcrud.currentPerfil" value="#{jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method['getAsConvertedString'][perfil.perfil][perfil.converter].jsfcrud_invoke}" />
                </h:commandLink>
                <br />
                <h:commandLink action="#{perfil.createSetup}" value="New Perfil" />
                <br />
                <h:commandLink action="#{perfil.listSetup}" value="Show All Perfil Items"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
    <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>New Perfil</title>
            <link rel="stylesheet" type="text/css" href="/unops3/faces/jsfcrud.css" />
        </head>
        <body>
            <h:panelGroup id="messagePanel" layout="block">
                <h:messages errorStyle="color: red" infoStyle="color: green" layout="table"/>
            </h:panelGroup>
            <h1>New Perfil</h1>
            <h:form>
                <h:inputHidden id="validateCreateField" validator="#{perfil.validateCreate}" value="value"/>
                <h:panelGrid columns="2">
                    <h:outputText value="IdPerfil:"/>
                    <h:inputText id="idPerfil" value="#{perfil.perfil.idPerfil}" title="IdPerfil" required="true" requiredMessage="The idPerfil field is required." />
                    <h:outputText value="Descripcion:"/>
                    <h:inputText id="descripcion" value="#{perfil.perfil.descripcion}" title="Descripcion" />
                    <h:outputText value="UsuarioCollection:"/>
                    <h:selectManyListbox id="usuarioCollection" value="#{perfil.perfil.jsfcrud_transform[jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.collectionToArray][jsfcrud_class['unops34.util.JsfUtil'].jsfcrud_method.arrayToList].usuarioCollection}" title="UsuarioCollection" size="6" converter="#{usuario.converter}" >
                        <f:selectItems value="#{usuario.usuarioItemsAvailableSelectMany}"/>
                    </h:selectManyListbox>

                </h:panelGrid>
                <br />
                <h:commandLink action="#{perfil.create}" value="Create"/>
                <br />
                <br />
                <h:commandLink action="#{perfil.listSetup}" value="Show All Perfil Items" immediate="true"/>
                <br />
                <br />
                <h:commandLink value="Index" action="welcome" immediate="true" />

            </h:form>
        </body>
    </html>
</f:view>

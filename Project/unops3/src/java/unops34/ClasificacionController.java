/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unops34;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.faces.FacesException;
import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import unops34.util.JsfUtil;
import unops34.util.PagingInfo;

/**
 *
 * @author Dell
 */
public class ClasificacionController {

    public ClasificacionController() {
        pagingInfo = new PagingInfo();
        converter = new ClasificacionConverter();
    }
    private Clasificacion clasificacion = null;
    private List<Clasificacion> clasificacionItems = null;
    private ClasificacionFacade jpaController = null;
    private ClasificacionConverter converter = null;
    private PagingInfo pagingInfo = null;
    @Resource
    private UserTransaction utx = null;
    @PersistenceUnit(unitName = "unops3PU")
    private EntityManagerFactory emf = null;

    public PagingInfo getPagingInfo() {
        if (pagingInfo.getItemCount() == -1) {
            pagingInfo.setItemCount(getJpaController().count());
        }
        return pagingInfo;
    }

    public ClasificacionFacade getJpaController() {
        if (jpaController == null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            jpaController = (ClasificacionFacade) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "clasificacionJpa");
        }
        return jpaController;
    }

    public SelectItem[] getClasificacionItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), false);
    }

    public SelectItem[] getClasificacionItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), true);
    }

    public Clasificacion getClasificacion() {
        if (clasificacion == null) {
            clasificacion = (Clasificacion) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentClasificacion", converter, null);
        }
        if (clasificacion == null) {
            clasificacion = new Clasificacion();
        }
        return clasificacion;
    }

    public String listSetup() {
        reset(true);
        return "clasificacion_list";
    }

    public String createSetup() {
        reset(false);
        clasificacion = new Clasificacion();
        return "clasificacion_create";
    }

    public String create() {
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().create(clasificacion);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Clasificacion was successfully created.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return listSetup();
    }

    public String detailSetup() {
        return scalarSetup("clasificacion_detail");
    }

    public String editSetup() {
        return scalarSetup("clasificacion_edit");
    }

    private String scalarSetup(String destination) {
        reset(false);
        clasificacion = (Clasificacion) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentClasificacion", converter, null);
        if (clasificacion == null) {
            String requestClasificacionString = JsfUtil.getRequestParameter("jsfcrud.currentClasificacion");
            JsfUtil.addErrorMessage("The clasificacion with id " + requestClasificacionString + " no longer exists.");
            return relatedOrListOutcome();
        }
        return destination;
    }

    public String edit() {
        String clasificacionString = converter.getAsString(FacesContext.getCurrentInstance(), null, clasificacion);
        String currentClasificacionString = JsfUtil.getRequestParameter("jsfcrud.currentClasificacion");
        if (clasificacionString == null || clasificacionString.length() == 0 || !clasificacionString.equals(currentClasificacionString)) {
            String outcome = editSetup();
            if ("clasificacion_edit".equals(outcome)) {
                JsfUtil.addErrorMessage("Could not edit clasificacion. Try again.");
            }
            return outcome;
        }
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().edit(clasificacion);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Clasificacion was successfully updated.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return detailSetup();
    }

    public String remove() {
        String idAsString = JsfUtil.getRequestParameter("jsfcrud.currentClasificacion");
        Integer id = new Integer(idAsString);
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().remove(getJpaController().find(id));
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Clasificacion was successfully deleted.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return relatedOrListOutcome();
    }

    private String relatedOrListOutcome() {
        String relatedControllerOutcome = relatedControllerOutcome();
        if ((ERROR)) {
            return relatedControllerOutcome;
        }
        return listSetup();
    }

    public List<Clasificacion> getClasificacionItems() {
        if (clasificacionItems == null) {
            getPagingInfo();
            clasificacionItems = getJpaController().findRange(new int[]{pagingInfo.getFirstItem(), pagingInfo.getFirstItem() + pagingInfo.getBatchSize()});
        }
        return clasificacionItems;
    }

    public String next() {
        reset(false);
        getPagingInfo().nextPage();
        return "clasificacion_list";
    }

    public String prev() {
        reset(false);
        getPagingInfo().previousPage();
        return "clasificacion_list";
    }

    private String relatedControllerOutcome() {
        String relatedControllerString = JsfUtil.getRequestParameter("jsfcrud.relatedController");
        String relatedControllerTypeString = JsfUtil.getRequestParameter("jsfcrud.relatedControllerType");
        if (relatedControllerString != null && relatedControllerTypeString != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            Object relatedController = context.getApplication().getELResolver().getValue(context.getELContext(), null, relatedControllerString);
            try {
                Class<?> relatedControllerType = Class.forName(relatedControllerTypeString);
                Method detailSetupMethod = relatedControllerType.getMethod("detailSetup");
                return (String) detailSetupMethod.invoke(relatedController);
            } catch (ClassNotFoundException e) {
                throw new FacesException(e);
            } catch (NoSuchMethodException e) {
                throw new FacesException(e);
            } catch (IllegalAccessException e) {
                throw new FacesException(e);
            } catch (InvocationTargetException e) {
                throw new FacesException(e);
            }
        }
        return null;
    }

    private void reset(boolean resetFirstItem) {
        clasificacion = null;
        clasificacionItems = null;
        pagingInfo.setItemCount(-1);
        if (resetFirstItem) {
            pagingInfo.setFirstItem(0);
        }
    }

    public void validateCreate(FacesContext facesContext, UIComponent component, Object value) {
        Clasificacion newClasificacion = new Clasificacion();
        String newClasificacionString = converter.getAsString(FacesContext.getCurrentInstance(), null, newClasificacion);
        String clasificacionString = converter.getAsString(FacesContext.getCurrentInstance(), null, clasificacion);
        if (!newClasificacionString.equals(clasificacionString)) {
            createSetup();
        }
    }

    public Converter getConverter() {
        return converter;
    }
    
}

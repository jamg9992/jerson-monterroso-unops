/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unops34;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 *
 * @author Dell
 */
public class ClasificacionConverter implements Converter {

    public Object getAsObject(FacesContext facesContext, UIComponent component, String string) {
        if (string == null || string.length() == 0) {
            return null;
        }
        Integer id = new Integer(string);
        ClasificacionController controller = (ClasificacionController) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "clasificacion");
        return controller.getJpaController().find(id);
    }

    public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
        if (object == null) {
            return null;
        }
        if (object instanceof Clasificacion) {
            Clasificacion o = (Clasificacion) object;
            return o.getIdClasificacion() == null ? "" : o.getIdClasificacion().toString();
        } else {
            throw new IllegalArgumentException("object " + object + " is of type " + object.getClass().getName() + "; expected type: unops34.Clasificacion");
        }
    }
    
}

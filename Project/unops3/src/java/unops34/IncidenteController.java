/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unops34;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.faces.FacesException;
import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import unops34.util.JsfUtil;
import unops34.util.PagingInfo;

/**
 *
 * @author Dell
 */
public class IncidenteController {

    public IncidenteController() {
        pagingInfo = new PagingInfo();
        converter = new IncidenteConverter();
    }
    private Incidente incidente = null;
    private List<Incidente> incidenteItems = null;
    private IncidenteFacade jpaController = null;
    private IncidenteConverter converter = null;
    private PagingInfo pagingInfo = null;
    @Resource
    private UserTransaction utx = null;
    @PersistenceUnit(unitName = "unops3PU")
    private EntityManagerFactory emf = null;

    public PagingInfo getPagingInfo() {
        if (pagingInfo.getItemCount() == -1) {
            pagingInfo.setItemCount(getJpaController().count());
        }
        return pagingInfo;
    }

    public IncidenteFacade getJpaController() {
        if (jpaController == null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            jpaController = (IncidenteFacade) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "incidenteJpa");
        }
        return jpaController;
    }

    public SelectItem[] getIncidenteItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), false);
    }

    public SelectItem[] getIncidenteItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), true);
    }

    public Incidente getIncidente() {
        if (incidente == null) {
            incidente = (Incidente) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentIncidente", converter, null);
        }
        if (incidente == null) {
            incidente = new Incidente();
        }
        return incidente;
    }

    public String listSetup() {
        reset(true);
        return "incidente_list";
    }

    public String createSetup() {
        reset(false);
        incidente = new Incidente();
        return "incidente_create";
    }

    public String create() {
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().create(incidente);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Incidente was successfully created.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return listSetup();
    }

    public String detailSetup() {
        return scalarSetup("incidente_detail");
    }

    public String editSetup() {
        return scalarSetup("incidente_edit");
    }

    private String scalarSetup(String destination) {
        reset(false);
        incidente = (Incidente) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentIncidente", converter, null);
        if (incidente == null) {
            String requestIncidenteString = JsfUtil.getRequestParameter("jsfcrud.currentIncidente");
            JsfUtil.addErrorMessage("The incidente with id " + requestIncidenteString + " no longer exists.");
            return relatedOrListOutcome();
        }
        return destination;
    }

    public String edit() {
        String incidenteString = converter.getAsString(FacesContext.getCurrentInstance(), null, incidente);
        String currentIncidenteString = JsfUtil.getRequestParameter("jsfcrud.currentIncidente");
        if (incidenteString == null || incidenteString.length() == 0 || !incidenteString.equals(currentIncidenteString)) {
            String outcome = editSetup();
            if ("incidente_edit".equals(outcome)) {
                JsfUtil.addErrorMessage("Could not edit incidente. Try again.");
            }
            return outcome;
        }
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().edit(incidente);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Incidente was successfully updated.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return detailSetup();
    }

    public String remove() {
        String idAsString = JsfUtil.getRequestParameter("jsfcrud.currentIncidente");
        Integer id = new Integer(idAsString);
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().remove(getJpaController().find(id));
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Incidente was successfully deleted.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return relatedOrListOutcome();
    }

    private String relatedOrListOutcome() {
        String relatedControllerOutcome = relatedControllerOutcome();
        if ((ERROR)) {
            return relatedControllerOutcome;
        }
        return listSetup();
    }

    public List<Incidente> getIncidenteItems() {
        if (incidenteItems == null) {
            getPagingInfo();
            incidenteItems = getJpaController().findRange(new int[]{pagingInfo.getFirstItem(), pagingInfo.getFirstItem() + pagingInfo.getBatchSize()});
        }
        return incidenteItems;
    }

    public String next() {
        reset(false);
        getPagingInfo().nextPage();
        return "incidente_list";
    }

    public String prev() {
        reset(false);
        getPagingInfo().previousPage();
        return "incidente_list";
    }

    private String relatedControllerOutcome() {
        String relatedControllerString = JsfUtil.getRequestParameter("jsfcrud.relatedController");
        String relatedControllerTypeString = JsfUtil.getRequestParameter("jsfcrud.relatedControllerType");
        if (relatedControllerString != null && relatedControllerTypeString != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            Object relatedController = context.getApplication().getELResolver().getValue(context.getELContext(), null, relatedControllerString);
            try {
                Class<?> relatedControllerType = Class.forName(relatedControllerTypeString);
                Method detailSetupMethod = relatedControllerType.getMethod("detailSetup");
                return (String) detailSetupMethod.invoke(relatedController);
            } catch (ClassNotFoundException e) {
                throw new FacesException(e);
            } catch (NoSuchMethodException e) {
                throw new FacesException(e);
            } catch (IllegalAccessException e) {
                throw new FacesException(e);
            } catch (InvocationTargetException e) {
                throw new FacesException(e);
            }
        }
        return null;
    }

    private void reset(boolean resetFirstItem) {
        incidente = null;
        incidenteItems = null;
        pagingInfo.setItemCount(-1);
        if (resetFirstItem) {
            pagingInfo.setFirstItem(0);
        }
    }

    public void validateCreate(FacesContext facesContext, UIComponent component, Object value) {
        Incidente newIncidente = new Incidente();
        String newIncidenteString = converter.getAsString(FacesContext.getCurrentInstance(), null, newIncidente);
        String incidenteString = converter.getAsString(FacesContext.getCurrentInstance(), null, incidente);
        if (!newIncidenteString.equals(incidenteString)) {
            createSetup();
        }
    }

    public Converter getConverter() {
        return converter;
    }
    
}

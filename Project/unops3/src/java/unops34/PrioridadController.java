/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unops34;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.faces.FacesException;
import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import unops34.util.JsfUtil;
import unops34.util.PagingInfo;

/**
 *
 * @author Dell
 */
public class PrioridadController {

    public PrioridadController() {
        pagingInfo = new PagingInfo();
        converter = new PrioridadConverter();
    }
    private Prioridad prioridad = null;
    private List<Prioridad> prioridadItems = null;
    private PrioridadFacade jpaController = null;
    private PrioridadConverter converter = null;
    private PagingInfo pagingInfo = null;
    @Resource
    private UserTransaction utx = null;
    @PersistenceUnit(unitName = "unops3PU")
    private EntityManagerFactory emf = null;

    public PagingInfo getPagingInfo() {
        if (pagingInfo.getItemCount() == -1) {
            pagingInfo.setItemCount(getJpaController().count());
        }
        return pagingInfo;
    }

    public PrioridadFacade getJpaController() {
        if (jpaController == null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            jpaController = (PrioridadFacade) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "prioridadJpa");
        }
        return jpaController;
    }

    public SelectItem[] getPrioridadItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), false);
    }

    public SelectItem[] getPrioridadItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), true);
    }

    public Prioridad getPrioridad() {
        if (prioridad == null) {
            prioridad = (Prioridad) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentPrioridad", converter, null);
        }
        if (prioridad == null) {
            prioridad = new Prioridad();
        }
        return prioridad;
    }

    public String listSetup() {
        reset(true);
        return "prioridad_list";
    }

    public String createSetup() {
        reset(false);
        prioridad = new Prioridad();
        return "prioridad_create";
    }

    public String create() {
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().create(prioridad);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Prioridad was successfully created.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return listSetup();
    }

    public String detailSetup() {
        return scalarSetup("prioridad_detail");
    }

    public String editSetup() {
        return scalarSetup("prioridad_edit");
    }

    private String scalarSetup(String destination) {
        reset(false);
        prioridad = (Prioridad) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentPrioridad", converter, null);
        if (prioridad == null) {
            String requestPrioridadString = JsfUtil.getRequestParameter("jsfcrud.currentPrioridad");
            JsfUtil.addErrorMessage("The prioridad with id " + requestPrioridadString + " no longer exists.");
            return relatedOrListOutcome();
        }
        return destination;
    }

    public String edit() {
        String prioridadString = converter.getAsString(FacesContext.getCurrentInstance(), null, prioridad);
        String currentPrioridadString = JsfUtil.getRequestParameter("jsfcrud.currentPrioridad");
        if (prioridadString == null || prioridadString.length() == 0 || !prioridadString.equals(currentPrioridadString)) {
            String outcome = editSetup();
            if ("prioridad_edit".equals(outcome)) {
                JsfUtil.addErrorMessage("Could not edit prioridad. Try again.");
            }
            return outcome;
        }
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().edit(prioridad);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Prioridad was successfully updated.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return detailSetup();
    }

    public String remove() {
        String idAsString = JsfUtil.getRequestParameter("jsfcrud.currentPrioridad");
        Integer id = new Integer(idAsString);
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().remove(getJpaController().find(id));
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Prioridad was successfully deleted.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return relatedOrListOutcome();
    }

    private String relatedOrListOutcome() {
        String relatedControllerOutcome = relatedControllerOutcome();
        if ((ERROR)) {
            return relatedControllerOutcome;
        }
        return listSetup();
    }

    public List<Prioridad> getPrioridadItems() {
        if (prioridadItems == null) {
            getPagingInfo();
            prioridadItems = getJpaController().findRange(new int[]{pagingInfo.getFirstItem(), pagingInfo.getFirstItem() + pagingInfo.getBatchSize()});
        }
        return prioridadItems;
    }

    public String next() {
        reset(false);
        getPagingInfo().nextPage();
        return "prioridad_list";
    }

    public String prev() {
        reset(false);
        getPagingInfo().previousPage();
        return "prioridad_list";
    }

    private String relatedControllerOutcome() {
        String relatedControllerString = JsfUtil.getRequestParameter("jsfcrud.relatedController");
        String relatedControllerTypeString = JsfUtil.getRequestParameter("jsfcrud.relatedControllerType");
        if (relatedControllerString != null && relatedControllerTypeString != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            Object relatedController = context.getApplication().getELResolver().getValue(context.getELContext(), null, relatedControllerString);
            try {
                Class<?> relatedControllerType = Class.forName(relatedControllerTypeString);
                Method detailSetupMethod = relatedControllerType.getMethod("detailSetup");
                return (String) detailSetupMethod.invoke(relatedController);
            } catch (ClassNotFoundException e) {
                throw new FacesException(e);
            } catch (NoSuchMethodException e) {
                throw new FacesException(e);
            } catch (IllegalAccessException e) {
                throw new FacesException(e);
            } catch (InvocationTargetException e) {
                throw new FacesException(e);
            }
        }
        return null;
    }

    private void reset(boolean resetFirstItem) {
        prioridad = null;
        prioridadItems = null;
        pagingInfo.setItemCount(-1);
        if (resetFirstItem) {
            pagingInfo.setFirstItem(0);
        }
    }

    public void validateCreate(FacesContext facesContext, UIComponent component, Object value) {
        Prioridad newPrioridad = new Prioridad();
        String newPrioridadString = converter.getAsString(FacesContext.getCurrentInstance(), null, newPrioridad);
        String prioridadString = converter.getAsString(FacesContext.getCurrentInstance(), null, prioridad);
        if (!newPrioridadString.equals(prioridadString)) {
            createSetup();
        }
    }

    public Converter getConverter() {
        return converter;
    }
    
}

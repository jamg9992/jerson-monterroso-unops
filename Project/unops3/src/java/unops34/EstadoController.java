/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unops34;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import javax.faces.FacesException;
import javax.annotation.Resource;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.transaction.UserTransaction;
import unops34.util.JsfUtil;
import unops34.util.PagingInfo;

/**
 *
 * @author Dell
 */
public class EstadoController {

    public EstadoController() {
        pagingInfo = new PagingInfo();
        converter = new EstadoConverter();
    }
    private Estado estado = null;
    private List<Estado> estadoItems = null;
    private EstadoFacade jpaController = null;
    private EstadoConverter converter = null;
    private PagingInfo pagingInfo = null;
    @Resource
    private UserTransaction utx = null;
    @PersistenceUnit(unitName = "unops3PU")
    private EntityManagerFactory emf = null;

    public PagingInfo getPagingInfo() {
        if (pagingInfo.getItemCount() == -1) {
            pagingInfo.setItemCount(getJpaController().count());
        }
        return pagingInfo;
    }

    public EstadoFacade getJpaController() {
        if (jpaController == null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            jpaController = (EstadoFacade) facesContext.getApplication().getELResolver().getValue(facesContext.getELContext(), null, "estadoJpa");
        }
        return jpaController;
    }

    public SelectItem[] getEstadoItemsAvailableSelectMany() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), false);
    }

    public SelectItem[] getEstadoItemsAvailableSelectOne() {
        return JsfUtil.getSelectItems(getJpaController().findAll(), true);
    }

    public Estado getEstado() {
        if (estado == null) {
            estado = (Estado) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentEstado", converter, null);
        }
        if (estado == null) {
            estado = new Estado();
        }
        return estado;
    }

    public String listSetup() {
        reset(true);
        return "estado_list";
    }

    public String createSetup() {
        reset(false);
        estado = new Estado();
        return "estado_create";
    }

    public String create() {
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().create(estado);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Estado was successfully created.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return listSetup();
    }

    public String detailSetup() {
        return scalarSetup("estado_detail");
    }

    public String editSetup() {
        return scalarSetup("estado_edit");
    }

    private String scalarSetup(String destination) {
        reset(false);
        estado = (Estado) JsfUtil.getObjectFromRequestParameter("jsfcrud.currentEstado", converter, null);
        if (estado == null) {
            String requestEstadoString = JsfUtil.getRequestParameter("jsfcrud.currentEstado");
            JsfUtil.addErrorMessage("The estado with id " + requestEstadoString + " no longer exists.");
            return relatedOrListOutcome();
        }
        return destination;
    }

    public String edit() {
        String estadoString = converter.getAsString(FacesContext.getCurrentInstance(), null, estado);
        String currentEstadoString = JsfUtil.getRequestParameter("jsfcrud.currentEstado");
        if (estadoString == null || estadoString.length() == 0 || !estadoString.equals(currentEstadoString)) {
            String outcome = editSetup();
            if ("estado_edit".equals(outcome)) {
                JsfUtil.addErrorMessage("Could not edit estado. Try again.");
            }
            return outcome;
        }
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().edit(estado);
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Estado was successfully updated.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return detailSetup();
    }

    public String remove() {
        String idAsString = JsfUtil.getRequestParameter("jsfcrud.currentEstado");
        Integer id = new Integer(idAsString);
        try {
            utx.begin();
        } catch (Exception ex) {
        }
        try {
            Exception transactionException = null;
            getJpaController().remove(getJpaController().find(id));
            try {
                utx.commit();
            } catch (javax.transaction.RollbackException ex) {
                transactionException = ex;
            } catch (Exception ex) {
            }
            if (transactionException == null) {
                JsfUtil.addSuccessMessage("Estado was successfully deleted.");
            } else {
                JsfUtil.ensureAddErrorMessage(transactionException, "A persistence error occurred.");
            }
        } catch (Exception e) {
            try {
                utx.rollback();
            } catch (Exception ex) {
            }
            JsfUtil.ensureAddErrorMessage(e, "A persistence error occurred.");
            return null;
        }
        return relatedOrListOutcome();
    }

    private String relatedOrListOutcome() {
        String relatedControllerOutcome = relatedControllerOutcome();
        if ((ERROR)) {
            return relatedControllerOutcome;
        }
        return listSetup();
    }

    public List<Estado> getEstadoItems() {
        if (estadoItems == null) {
            getPagingInfo();
            estadoItems = getJpaController().findRange(new int[]{pagingInfo.getFirstItem(), pagingInfo.getFirstItem() + pagingInfo.getBatchSize()});
        }
        return estadoItems;
    }

    public String next() {
        reset(false);
        getPagingInfo().nextPage();
        return "estado_list";
    }

    public String prev() {
        reset(false);
        getPagingInfo().previousPage();
        return "estado_list";
    }

    private String relatedControllerOutcome() {
        String relatedControllerString = JsfUtil.getRequestParameter("jsfcrud.relatedController");
        String relatedControllerTypeString = JsfUtil.getRequestParameter("jsfcrud.relatedControllerType");
        if (relatedControllerString != null && relatedControllerTypeString != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            Object relatedController = context.getApplication().getELResolver().getValue(context.getELContext(), null, relatedControllerString);
            try {
                Class<?> relatedControllerType = Class.forName(relatedControllerTypeString);
                Method detailSetupMethod = relatedControllerType.getMethod("detailSetup");
                return (String) detailSetupMethod.invoke(relatedController);
            } catch (ClassNotFoundException e) {
                throw new FacesException(e);
            } catch (NoSuchMethodException e) {
                throw new FacesException(e);
            } catch (IllegalAccessException e) {
                throw new FacesException(e);
            } catch (InvocationTargetException e) {
                throw new FacesException(e);
            }
        }
        return null;
    }

    private void reset(boolean resetFirstItem) {
        estado = null;
        estadoItems = null;
        pagingInfo.setItemCount(-1);
        if (resetFirstItem) {
            pagingInfo.setFirstItem(0);
        }
    }

    public void validateCreate(FacesContext facesContext, UIComponent component, Object value) {
        Estado newEstado = new Estado();
        String newEstadoString = converter.getAsString(FacesContext.getCurrentInstance(), null, newEstado);
        String estadoString = converter.getAsString(FacesContext.getCurrentInstance(), null, estado);
        if (!newEstadoString.equals(estadoString)) {
            createSetup();
        }
    }

    public Converter getConverter() {
        return converter;
    }
    
}
